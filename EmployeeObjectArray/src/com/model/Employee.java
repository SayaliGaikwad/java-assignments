package com.model;

public class Employee {

	private int employeeNumber;
	private String employeeName;
	private float salary;

	public Employee() {
		super();
		System.out.println("Default constructor of parent class is called first");
	}

	public void sayHello() {
		
		System.out.println("Employee sayHello method");
	}
	
	public Employee(int employeeNumber, String employeeName, float salary) {
		super();
		this.employeeNumber = employeeNumber;
		this.employeeName = employeeName;
		this.salary = salary;
	}

	public Employee(int employeeNumber, String employeeName) {
		super();
		this.employeeNumber = employeeNumber;
		this.employeeName = employeeName;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	
}
