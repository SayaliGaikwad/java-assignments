package com.main;

import com.model.Employee;
import com.service.EmployeeService;

public class ArrayMain {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 1010.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);

		Employee[] employees = new Employee[3]; // collection of object

		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;

		System.out.println("Iterate");
		for (int i = 0; i < employees.length; i++) {
			System.out.println("Employee number : " + employees[i].getEmployeeNumber());
			System.out.println("Employee Name : " + employees[i].getEmployeeName());
			System.out.println("Employee salary : " + employees[i].getSalary());
		}

		EmployeeService empService = new EmployeeService();
		Employee emp = empService.updateSalary(employee2, 1000f);
		System.out.println(emp.getSalary());

	}

}
