package com.main;

import com.model.ContractEmployee;
import com.model.Employee;

public class ContractEmployeeMain{

	public static void main(String[] args) {
		
		Employee employee = new Employee();
		
		ContractEmployee contractEmployee = new ContractEmployee();
		contractEmployee.sayHello();
		employee.sayHello();
		
		contractEmployee.setEmployeeNumber(5198);
		contractEmployee.setEmployeeName("Contract Employee");
		
		System.out.println(contractEmployee.getEmployeeNumber());
		System.out.println(contractEmployee.getEmployeeName());
	}

}
