package com.main;

import com.model.Department;
import com.model.Employee;

public class OneToManyMapping {

	public static void main(String[] args) {
		
		Department department= new Department();
		
		Employee employee1= new Employee(10, "Ten");
		Employee employee2= new Employee(20,"Twenty");
		Employee[] employees= new Employee[2];
		employees[0]= employee1;
		employees[1]= employee2;
				
		department.setDeptId(123);
		department.setDeptName("Developement");
		department.setEmployee(employees);
		
		
		
		//Iterate Employee
		Employee[] employeeArray= department.getEmployee();
		for (int i = 0; i < employeeArray.length; i++) {
			System.out.print(i+1+") ");
			System.out.println("Department ID= "+department.getDeptId());
			System.out.println("Department Name= "+department.getDeptName());
			System.out.println("Employee no= "+employeeArray[i].getEmployeeNumber());
			System.out.println("Employee name= "+employeeArray[i].getEmployeeName());
		}
		
	}	

}
