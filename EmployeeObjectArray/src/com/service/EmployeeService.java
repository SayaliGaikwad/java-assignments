package com.service;

import com.model.Employee;

public class EmployeeService {

	public Employee updateSalary(Employee employee, float salaryHike) {
		employee.setSalary(employee.getSalary() + salaryHike); // old + hike
		return employee;
	}
}
