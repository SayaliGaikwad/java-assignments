package com.main;

import com.service.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {

		Calculator calculator = new Calculator();

		try {
			int var1 = Integer.parseInt(args[0]);
			int var2 = Integer.parseInt(args[1]);

			int ans = calculator.div(var1, var2);
			System.out.println("Answer of division is: " + ans);

		}

		catch (java.lang.Exception e) {
			System.err.println("Solution: " + e.getMessage());
		}

		finally {
			if (args[2] != null) {
				System.err.println("Number of input arguements exceeded ");
			}
		}

	}
}
