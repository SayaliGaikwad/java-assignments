package com.hcl.ppusermicroservice.service;

import java.util.Set;

import com.hcl.ppusermicroservice.dto.PetDto;
import com.hcl.ppusermicroservice.exception.PetPeersException;
import com.hcl.ppusermicroservice.model.User;
import com.hcl.ppusermicroservice.validator.LoginValidator;
import com.hcl.ppusermicroservice.validator.UserValidator;

public interface UserService {

	public User addUser(UserValidator userValidator) throws PetPeersException;

	public User updateUser(Long userId, UserValidator userValidator) throws PetPeersException;

	 public Set<User> listUsers() throws PetPeersException;

	public User getUserById(Long userId) throws PetPeersException;

	public Set<PetDto> getMyPets(Long userId) throws PetPeersException;

	public PetDto buyPet(Long userId, Long petId) throws PetPeersException;
	
	public Set<PetDto> loginUser(LoginValidator loginRequest) throws PetPeersException;

	void removeUser(Long userId) throws PetPeersException;

}

