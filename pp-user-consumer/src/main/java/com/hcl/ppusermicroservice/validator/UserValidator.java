package com.hcl.ppusermicroservice.validator;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserValidator implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1182902421716658644L;

	@NotNull(message = "userName can't be null")
	@NotEmpty(message = "userName can't be empty")
	private String userName;

	@NotNull(message = "password can't be null")
	@NotEmpty(message = "password can't be empty")
	private String userPassword;

	@NotNull(message = "confirm password can't be null")
	@NotEmpty(message = "confirm password can't be empty")
	private String confirmPassword;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}

