package com.hcl.ppusermicroservice.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ppusermicroservice.model.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public abstract User findByUserName(String userName);

	public abstract User findByUserNameAndUserPassword(String userName, String password);

}

