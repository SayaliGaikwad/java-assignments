package com.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.main.MainApplication;

public class EmployeeService {
	private static final Logger LOGGER= LogManager.getLogger(EmployeeService.class);
	
	public int add(int a ,int b)
	{
		return a+b;
	}
	public int sub(int a ,int b)
	{
		return a-b;
	}
}
