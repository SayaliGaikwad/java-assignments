package com.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.EmployeeService;

public class MainApplication {
	private static final Logger LOGGER = LogManager.getLogger(MainApplication.class);

	public static void main(String[] args) {
		LOGGER.info("You are in Main()");
		LOGGER.warn("Warning");
		LOGGER.error("Oops, an error occurred!");

		EmployeeService employeeService = new EmployeeService();
		int answer = employeeService.add(10, 20);
		System.out.println("Result of addition = " + answer);
		int difference = employeeService.sub(10, 20);
		System.out.println("Result of substraction = " + difference);
		LOGGER.info("Data received from service");
		System.out.println("The End!");
	}

}
