package com.service;

import com.model.RichEmployee;

public class RichEmployeeService {

	public RichEmployee compareSalary(RichEmployee richEmployee1, RichEmployee richEmployee2) {
		if (richEmployee1.getEmployeeSalary() > richEmployee2.getEmployeeSalary())
			return richEmployee1;
		else
			return richEmployee2;
	}
}
