package com.main;

import com.model.WelcomeName;

public class WelcomeNameMain {

	public static void main(String[] args) {
		WelcomeName welcomeName = new WelcomeName();

		welcomeName.setName("HCL Technologies");

		System.out.println("Welcome to " + welcomeName.getName()+"!");

	}

}
