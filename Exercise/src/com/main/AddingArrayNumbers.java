package com.main;

/**
 * @author sayali.gaikwad
 *
 */
// Create and array and sum the numbers

public class AddingArrayNumbers {

	public static void main(String[] args) {
		int[] numbers = { 50, -360, 10, -100 };
		int sum = 0;
		for (int i = 0; i < numbers.length; i++) {
			sum = sum + numbers[i];
		}

		System.out.println("Sum of the " + numbers.length + " numbers in the array is " + sum);
	}

}
