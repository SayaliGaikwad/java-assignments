package com.main;

import com.model.RichEmployee;
import com.service.RichEmployeeService;

/**
 * @author sayali.gaikwad
 *
 */

// Create two employees and check who is drawing higher salary

public class RichEmployeeMain {

	public static void main(String[] args) {

		RichEmployee richEmployee1 = new RichEmployee(5198, "Micky", 10000.69f);
		RichEmployee richEmployee2 = new RichEmployee(6198, "Mini", 80000f);

		System.out.println("Mini draws " + richEmployee1.getEmployeeSalary() + " INR");
		System.out.println("Micky draws " + richEmployee2.getEmployeeSalary() + " INR");

		RichEmployeeService richEmployeeService = new RichEmployeeService();
		RichEmployee emp = richEmployeeService.compareSalary(richEmployee1, richEmployee2);
		System.out.println(emp.getEmployeeName() + " draws higher salary = " + emp.getEmployeeSalary() + " INR");

		richEmployee1 = null;
		richEmployee2 = null;
		emp = null;
	}

}
