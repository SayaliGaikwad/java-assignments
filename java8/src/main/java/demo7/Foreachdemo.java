package demo7;

import java.util.ArrayList;
import java.util.List;

public class Foreachdemo {
	public static void main(String[] args) {
		department department1 = new department(10, "Development");
		department department2 = new department(20, "Testing");
		department department3 = new department(30, "ITSD");

		List<department> departments = new ArrayList<>();
		departments.add(department1);
		departments.add(department2);
		departments.add(department3);

		departments.forEach((dep) -> { // functional programming
			System.out.println(dep.getDeptNo());
			System.out.println(dep.getDeptName());
		});
	}

}
