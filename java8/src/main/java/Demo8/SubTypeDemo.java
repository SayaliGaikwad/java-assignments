package Demo8;

import java.util.ArrayList;
import java.util.List;

public class SubTypeDemo {

	public static void main(String[] args) {
		List<? super Number> list = new ArrayList();
		list.add(new Integer(55));
		list.add(new Float(55.5));
		for (Object num : list) {
			System.out.println(num);
		}

	}

}
