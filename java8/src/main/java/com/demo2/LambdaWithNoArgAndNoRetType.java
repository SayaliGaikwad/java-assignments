package com.demo2;

public class LambdaWithNoArgAndNoRetType {
	public static void main(String[] args) {
		MyFunctionalInterface myFunctionalInterface = () -> System.out.println("Welcome to Lambda Expression");
		// pointer
		myFunctionalInterface.display();
		LambdaWithNoArgAndNoRetType.sayHello();
		LambdaWithNoArgAndNoRetType.sayWorld(myFunctionalInterface);
	}

	public static void sayHello() {
		System.out.println("sayHello()");
	}

	public static void sayWorld(MyFunctionalInterface func) {
		func.display();
		System.out.println("sayWorld()");
	}
}
