package com.constructormethodreference;

public class MainApp {

	public static void main(String[] args) {
		MyFuncInterface myFuncInterface = Hello::new;
		myFuncInterface.match("World");
	}

}
