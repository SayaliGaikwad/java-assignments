package com.demo5;

public class LambdaWithArgAndRet {

	public static void main(String[] args) {
		MyFunctionalInterface myFunctionalInterface = (String str) -> {
			return "Welcome " + str;
		};
		String capture = myFunctionalInterface.sayHello("Dear");
		System.out.println(capture);
	}

}
