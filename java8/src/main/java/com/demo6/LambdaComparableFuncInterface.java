package com.demo6;

public class LambdaComparableFuncInterface {

	public static void main(String[] args) {
		Comparable<Employee> comparable = (var) -> {
			System.out.println("Hello");
			return (int) var.getSalary() + 1000;
		};
		int ans = comparable.compareTo(new Employee(20, "Twenty", 2020.2f));
		System.out.println(ans);
		System.out.println("Bye");
	}

}
