package com.demo6;

import java.util.function.Function;

public class LambdaFunctionInterfaceDemo {

	public static void main(String[] args) {
		Function<Employee, String> function = (e) -> {
			return "Welcome " + e.getEmpName();
		};
		String str = function.apply(new Employee(10, "One", 1111.1f));
		System.out.println(str);
	}

}
