package com.demo6;

import java.util.function.BiConsumer;

//s.. one is BiConsumer InterfaceJava 8 has pre defined functional interface
public class BiConsumerInterfaceDemo {

	public static void main(String[] args) {
		BiConsumer<Integer, String> biConsumer = (Integer i, String s) -> {
			System.out.println("Integer value = " + (i + 20));
			System.out.println("String value = " + s);
		};
		biConsumer.accept(200, "Hello");
		System.out.println("Bye");
	}

}
