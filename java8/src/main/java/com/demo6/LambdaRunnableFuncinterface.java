package com.demo6;

public class LambdaRunnableFuncinterface {

	public static void main(String[] args) {
		Runnable runnable = () -> {
			System.out.println("Welcome runnable Interface");
		};
		runnable.run();
		System.out.println("the end");
	}

}
