package com.instancemethodreference;

public class InstanceMethodDemo {
	public void hello() {
		System.out.println("Hello");
	}

	public static void main(String[] args) {
		MyFunctionalInterface myFunctionalInterface = () -> {
			System.out.println("Using Lambda Exp");
		};

		InstanceMethodDemo instanceMethodDemo = new InstanceMethodDemo();
		MyFunctionalInterface myFunctionalInterface2 = instanceMethodDemo::hello; // method reference
		myFunctionalInterface2.display();
	}

}
