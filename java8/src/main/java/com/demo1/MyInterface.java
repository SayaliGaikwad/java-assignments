package com.demo1;

public interface MyInterface {
	public static final float PI = 3.1514f;

	public abstract int add(int a, int b);

	public default int sub(int a, int b) { // method declaration + def==logic
		return a - b;
	}

	public static int mul(int a, int b) {
		return a * b;
	}

}
