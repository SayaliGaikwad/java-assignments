package com.demo1;

public interface AnotherInterface {
	public default int sub(int a, int b) { // method declaration + def==logic
		return a - b;
	}
}
