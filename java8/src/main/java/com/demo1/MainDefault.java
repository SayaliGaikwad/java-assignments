package com.demo1;

public class MainDefault {

	public static void main(String[] args) {
		MyInterface myInterface = new MyInterfaceImpl();
		int ans = myInterface.add(4, -6);
		System.out.println("Result is : " + ans);
		System.out.println("Static final VARIABLE: " + MyInterface.PI);
		System.out.println("Difference= " + myInterface.sub(5, 2));
		System.out.println("Product= " + MyInterface.mul(10, 20)); // static-->ClassNamemethodrName()
	}

}
