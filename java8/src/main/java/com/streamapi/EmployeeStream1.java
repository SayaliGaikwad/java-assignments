package com.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeStream1 {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 10123.10f, 'A');
		Employee employee2 = new Employee(20, "Twenty", 2020.10f, 'B');
		Employee employee3 = new Employee(40, "Forty", 4030.10f, 'A');

		List<Employee> employeeList = new ArrayList();
		employeeList.add(employee1);
		employeeList.add(employee2);
		employeeList.add(employee3);

		long data = employeeList.stream().count();
		System.out.println("Count = " + data);

		// for each-- Terminal operation
		employeeList.stream().forEach((abc) -> {

			System.out.println("Employee name is "+abc.getEmpName());
		});

		// Intermediate operation
		employeeList.stream().filter((arg) -> arg.getEmpName().startsWith("T")).forEach((getData) -> 
		{
			System.out.println("Employee name starting with T "+getData.getEmpName());
		});
		
		//fetch all emp in band a and give hike
		/*long result=employeeList.stream().filter((arg2) -> arg2.getBand().equals("A")).map((arg3)->arg3.getSalary()+1000).count();
		
		System.out.println(result);
	*/
	};

}
