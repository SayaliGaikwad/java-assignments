package com.staticmethodreference;

public class StaticMetRefMain {

	public static void main(String[] args) {
		MyStaticFunInterface myStaticFunInterface = (aa, bb) -> {
			return aa + bb;
		};
		System.out.println(myStaticFunInterface.add(10, 20));

		// Reference to static method
		MyStaticFunInterface myStaticFunInterface2 = MyStaticClass::adding;
		int ans = myStaticFunInterface2.add(20, 30);
		System.out.println("Using Static Method Reference " + ans);
	}

}
