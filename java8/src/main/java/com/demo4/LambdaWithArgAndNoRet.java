package com.demo4;

public class LambdaWithArgAndNoRet {

	public static void main(String[] args) {
		MyFunctionalInterface myFunctionalInterface = (var) -> // (datatype var) is opt
		{
			System.out.println("Welcome to Lambda Expression with arg " + var);
			System.out.println("Welcome to Lambda Expression with arg " + var);
		};
		myFunctionalInterface.display(10);

	}
}