package com.main;

import com.model.Address;
import com.model.Department;
import com.model.Employee;

public class EmployeeAddressMain {

	public static void main(String[] args) {

		// employee, department and address objects
		Department department = new Department();
		department.setDeptId(123);
		department.setDeptName("Developement");

		Employee[] employees = new Employee[2];
		Employee employee1 = new Employee(10, "Employee1");
		Employee employee2 = new Employee(20, "Employee2");
		employees[0] = employee1;
		employees[1] = employee2;

		Address[] addresses = new Address[2];
		Address address1 = new Address("Belgaum", "HCL Bengaluru");
		Address address2 = new Address("Kolhapur", "HCL Chennai");
		addresses[0] = address1;
		addresses[1] = address2;

		department.setEmployee(employees);

		
		Address[] addressArray1 = employee1.getAddresses();

		employee2.setAddresses(addresses);
		Address[] addressArray2 = employee2.getAddresses();

		Employee[] employeeArray = department.getEmployee();

		for (int i = 0; i < employeeArray.length; i++) {

			System.out.print(+1 + ") ");
			System.out.println("Department ID= " + department.getDeptId());
			System.out.println("Department Name= " + department.getDeptName());
			System.out.println("Employee ID= " + employeeArray[i].getEmpId());
			System.out.println("Employee Name= " + employeeArray[i].getEmpName());

		}

		for (int j = 0; j < addressArray1.length; j++) {
			System.out.println("Home address is " + addressArray1[j].getHomeAddress());
			System.out.println("Office address is " + addressArray1[j].getOfficeAddress());
		}

	}
}