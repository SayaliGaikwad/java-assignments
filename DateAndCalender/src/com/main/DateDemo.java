package com.main;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDemo {

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println("Today's day, date and time are:- " + date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		System.out.println("Today's date is:- " + simpleDateFormat.format(date));

	}

}
