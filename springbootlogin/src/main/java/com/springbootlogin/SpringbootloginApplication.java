package com.springbootlogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author sayali.gaikwad
 *
 */

@SpringBootApplication
//@EnableSwagger2
public class SpringbootloginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootloginApplication.class, args);
	}

}
