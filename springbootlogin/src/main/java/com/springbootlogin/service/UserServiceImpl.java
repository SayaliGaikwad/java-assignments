package com.springbootlogin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springbootlogin.model.User;
import com.springbootlogin.repository.UserRepository;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public User createuser(User user) {
		User user1 = null;
		String mustContain = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20}$";
		Pattern pattern = Pattern.compile(mustContain);
		Matcher matcher = pattern.matcher(user.getPassword());
		if (matcher.matches()) {
			user1 = userRepository.save(user);
		}

		/*
		 * if(user.getPassword().length()>8) { user1=userRepository.save(user); } return
		 * user1;
		 */
		return user1;
	}

	@Override
	@Transactional
	public String loginUser(String username, String password) {
		User user2 = null;
		String name = null;
		if (userRepository.equals(username) && userRepository.equals(password)) {
			name = user2.getFirstName();
		}
		return name;
	}

}
