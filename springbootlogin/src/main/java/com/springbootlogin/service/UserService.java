package com.springbootlogin.service;

import org.springframework.stereotype.Service;

import com.springbootlogin.model.User;

public interface UserService {
	public abstract User createuser(User user);
	public abstract String loginUser(String username, String password);
	
}
