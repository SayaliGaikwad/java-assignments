package com.springbootlogin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootlogin.model.User;
import com.springbootlogin.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping(value = "hello")
	public String sayHello() {
		return "Welcome to Spring boot";
	}

	@PostMapping(value = "create")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		userService.createuser(user);
		return new ResponseEntity<User>(user, HttpStatus.CREATED);
	}

	@GetMapping(value = "validate/{username}/{password}")
	public String validateUser(@PathVariable String username, String password) {
		String name = null;
		userService.loginUser(username, password);
		return "Welcome " + name;
	}
}
