package com.springbootlogin.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springbootlogin.model.User;



@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

}
