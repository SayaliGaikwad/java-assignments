package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data

public class LoginValidator implements Serializable{
	
	private static final long serialVersionUID = 5016789534493637833L;

	@NotNull(message= "Username cannot be null")
	@NotEmpty(message= "Username cannot be empty")
	private String userName;

	@NotNull(message= "Password cannot be null")
	@NotEmpty(message= "Password cannot be empty")
	private String userPassword;
	

}
