package com.hcl.pp.validator;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data

public class UserValidator implements Serializable{
	
	private static final long serialVersionUID = -621158079360714993L;

	@NotNull(message= "Username cannot be null")
	@NotEmpty(message= "Username cannot be empty")
	private String userName;
	
	@NotNull(message= "User Password cannot be null")
	@NotEmpty(message= "User Password cannot be empty")
	private String userPassword;
	
	@NotNull(message= "Confirm Password feild cannot be null")
	@NotEmpty(message= "Confirm Password feild cannot be empty")
	private String confirmPassword;
}
