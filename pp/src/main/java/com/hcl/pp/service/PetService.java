package com.hcl.pp.service;

import java.util.Set;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.validator.PetValidator;

public interface PetService {

	public Pet savePet(PetValidator petValidator) throws PetPeersException; // to add pets

	public Set<Pet> getAllPets() throws PetPeersException;

	public Set<Pet> getPetsByOwnerId(User owner) throws PetPeersException; // pets owned by user

	public Pet getPetById(Long id) throws PetPeersException; // a particular pet

	public Pet savePetWithOwner(Pet petExists) throws PetPeersException;
}