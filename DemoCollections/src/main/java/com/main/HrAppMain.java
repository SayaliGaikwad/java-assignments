package com.main;

import java.util.HashSet;
import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class HrAppMain {

	public static void main(String[] args) {

		Employee employee1 = new Employee(10, "Ten", 1010.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);

		Set<Employee> employees = new HashSet();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);

		EmployeeService employeeService = new EmployeeServiceImpl();

		try {
			Employee employeeById = employeeService.searchEmployeeById(employees, 10);
			System.out.println("Details of the employee with employee ID= '" + employeeById.getEmpId() + "' is as follows:-");
			System.out.println("Employee ID= " + employeeById.getEmpId());
			System.out.println("Employee name= " + employeeById.getEmpName());
			System.out.println("Employee salary= " + employeeById.getSalary());
			System.out.println("");
		}

		catch (EmployeeException e) {
			System.err.println(e.getMessage());
		}

		try {
			Employee employeeByName = employeeService.searchEmployeeByName(employees, "Twenty");
			System.out.println("Details of the employee with employee name: '" + employeeByName.getEmpName()+ "' is as follows:-");
			System.out.println("Employee ID= " + employeeByName.getEmpId());
			System.out.println("Employee name= " + employeeByName.getEmpName());
			System.out.println("Employee salary= " + employeeByName.getSalary());
		}

		catch (EmployeeException e) {

			System.err.println(e.getMessage());
		}
	}

}
