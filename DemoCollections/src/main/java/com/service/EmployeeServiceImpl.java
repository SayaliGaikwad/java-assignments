package com.service;

import java.util.Iterator;
import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee searchEmployeeById(Set<Employee> set, int id) throws EmployeeException {
		Employee dummyEmployeeById = null;
		for (Employee employeeById : set) {
			if (employeeById.getEmpId() == id) {
				dummyEmployeeById = employeeById;
			} else {
				throw new EmployeeException("No such employee for employee number: " + id);
			}
		}
		return dummyEmployeeById;
	}

	@Override
	public Employee searchEmployeeByName(Set<Employee> set, String eName) throws EmployeeException {
		Employee dummyEmployeeByName = null;
		for (Employee employeeByName : set) {
			if (employeeByName.getEmpName() == eName) {
				dummyEmployeeByName = employeeByName;
			} 
			else {
				throw new EmployeeException("No such employee for employee name: " + eName);
			}
		}
		return dummyEmployeeByName;

	}
}
