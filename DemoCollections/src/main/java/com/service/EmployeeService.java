package com.service;

import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

public interface EmployeeService {

	Employee searchEmployeeById(Set<Employee> set, int id) throws EmployeeException;

	Employee searchEmployeeByName(Set<Employee> set, String eName) throws EmployeeException;

}
