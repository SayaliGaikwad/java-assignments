package com.springbootdemo1.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springbootdemo1.model.Address;
import com.springbootdemo1.repository.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	private AddressRepository addressRepository;

	@Override
	@Transactional
	public Address addAddress(Address address) {
		return addressRepository.save(address);
	}

	@Override
	@Transactional
	public Address readAddressByDoorNo(int doorNo) {
		Address address = null;
		Optional<Address> optionalAddress = addressRepository.findById(doorNo);
		if (optionalAddress.isPresent()) {
			address = optionalAddress.get();
		}
		return address;
	}

	@Override
	@Transactional
	public Address alterAddress(Address address) {
		return addressRepository.save(address);
	}

	@Override
	@Transactional
	public Address removeAddressByDoorNo(int doorNo) {
		Address address = null;
		addressRepository.deleteById(doorNo);
		return address;
	}

	@Override
	public String getStateById(int doorNo) {
		return addressRepository.getStateByDoorNo(doorNo);
	}

	

}
