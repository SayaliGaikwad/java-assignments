package com.springbootdemo1.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springbootdemo1.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer>{

	@Query("SELECT add.state FROM Address as add WHERE doorNo=  :id")
	public abstract String getStateByDoorNo(@Param("id") Integer doorNo);

}
