package com.springbootdemo1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo1.model.Address;
import com.springbootdemo1.service.AddressService;

@RestController
@RequestMapping(value="addressmain")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@GetMapping(value = "hello")
	public String sayHello() {
		return "Welcome to Spring boot";
	}

	@PostMapping(value = "create")
	public ResponseEntity<Address> createAddress(@RequestBody Address address) {
		Address address1 = null;
		if (address != null) {
			address1 = addressService.addAddress(address);
		}
		return new ResponseEntity<Address>(address1, HttpStatus.CREATED);
	}

	@GetMapping(value = "readbydoorno/{doorNo}")
	// public ResponseEntity<Address> readAddressByDoorNo(@PathVariable int doorNo){
	public Address readAddressByDoorNo(@PathVariable int doorNo) {
		Address address2 = null;
		if (doorNo > 0) {
			address2 = addressService.readAddressByDoorNo(doorNo);
		}
		return address2;
		// return new ResponseEntity<Address>(address2, HttpStatus.FOUND);
	}

	@PutMapping(value = "update")
	public ResponseEntity<Address> updateAddress(@RequestBody Address address) {
		Address address3 = null;
		address3 = addressService.alterAddress(address);
		return new ResponseEntity<Address>(address3, HttpStatus.CREATED);
	}

	@DeleteMapping(value = "delete/{doorNo}")
	// public ResponseEntity<Address> deleteAddressByDoorNo(@PathVariable int doorNo) {
	public Address deleteAddressByDoorNo(@PathVariable int doorNo) {
		Address address4 = null;
		address4 = addressService.removeAddressByDoorNo(doorNo);

		// return new ResponseEntity<Address>(address4, HttpStatus.GONE);
		return address4;

	}

	@GetMapping("custom/{dnum}")
	public ResponseEntity<String> customQuery(@PathVariable("dnum") int doorNo)
	{
		String str= addressService.getStateById(doorNo);
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}
}
