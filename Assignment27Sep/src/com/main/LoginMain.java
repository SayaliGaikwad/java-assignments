package com.main;
//WAP to validate Login

import java.util.Scanner;

import com.model.User;
import com.service.ValidateUserLogin;

/**
 * @author sayali.gaikwad
 *
 */
public class LoginMain {

	public static void main(String[] args) {

		User user = new User();
		// user.setUserId(1234);
		// user.setUsername("Geeta");
		// user.setPassword("hello123");

		ValidateUserLogin validateUserLogin = new ValidateUserLogin();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter your User ID");
		int id = scanner.nextInt();
		System.out.println("Please enter your Username");
		String userna = scanner.next();
		System.out.println("Please enter your password carefully");
		String pwd = scanner.next();

		ValidateUserLogin.no = id;
		ValidateUserLogin.pass = pwd;
		ValidateUserLogin.usern = userna;

		validateUserLogin.validateUserIdAndPassword(1234, "hello123");

		user = null;
		validateUserLogin = null;
		scanner = null;

	}

}
