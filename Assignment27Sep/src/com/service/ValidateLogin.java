package com.service;

public interface ValidateLogin {

	public abstract void validateUserIdAndPassword(int userId, String password);
}
