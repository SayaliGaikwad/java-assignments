package com.main;

import com.service.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();

		// memory address where reference is pointing
		System.out.println(calculator);

		int addition = calculator.add(10, 20);
		System.out.println("Result of addition = " + addition);

		int difference = calculator.sub(10, 20);
		System.out.println("Result of substraction = " + difference);

		double product = calculator.mul(200, 300);
		System.out.println("Result of multiplication = " + product);

		float portion = calculator.div(250f, 15f);
		System.out.println("Result of division = " + portion);
		
		calculator= null; //de-reference
	}

}
