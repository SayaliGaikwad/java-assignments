package com.main;

import com.model.Employee;

public class EmployeeMain {

	public static void main(String[] args) {

		Employee employee = new Employee();

		// print default values
		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		System.out.println(employee.getSalary());

		// set values
		employee.setEmpNo(123);
		employee.setEmpName("ABC");
		employee.setSalary(12345f);

		// print assigned values
		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		System.out.println(employee.getSalary());

	}

}
