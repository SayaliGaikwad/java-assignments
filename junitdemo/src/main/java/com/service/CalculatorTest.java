package com.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {

	static Calculator calculator = null;

	@BeforeClass
	public static void startTest() {
		calculator = new Calculator();
		System.out.println("init().....@BeforeClass will be executed only once.");
	}

	@Test
	public void testAdd() {

		assertEquals(8, calculator.add(3, 5));
	}

	@Test
	public void testAdd2() {

		assertEquals(-2, calculator.add(3, -5));
	}

	@AfterClass
	public static void stopTest() {
		calculator = new Calculator();
		System.out.println("close()....@AfterClass will be executed only once.");
	}
}
