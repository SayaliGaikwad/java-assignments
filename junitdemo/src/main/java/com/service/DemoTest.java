package com.service;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class DemoTest {

	@Test

	public void testSayHello() {
		Demo demo = new Demo();
		assertEquals("Welcome", demo.sayHello());
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	@Ignore
	public void testGetName() {
		Demo demo = new Demo();
		assertEquals("Welcome HCL", demo.getName("HCL"));
	}

}
