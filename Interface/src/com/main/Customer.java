package com.main;

import com.model.Airtel;
import com.model.DataRecharge;
import com.model.Jio;
import com.model.Recharge;
//Learning Interfaces

public class Customer {

	public static void main(String[] args) {

		Recharge rechargeAirtel = new Airtel();
		Recharge rechargeJio = new Jio();

		DataRecharge dataRechargeAirtel = new Airtel();
		DataRecharge dataRechargeJio = new Jio();

		rechargeAirtel.rs50();
		rechargeAirtel.rs100();

		System.out.println(dataRechargeAirtel.dataFor50() + "--Airtel");
		System.out.println(dataRechargeAirtel.dataFor100() + "--Airtel");

		rechargeJio.rs50();
		rechargeJio.rs100();

		System.out.println(dataRechargeJio.dataFor50() + "--Jio");
		System.out.println(dataRechargeJio.dataFor100() + "--Jio");

	}

}
