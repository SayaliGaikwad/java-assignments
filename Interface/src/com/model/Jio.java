package com.model;

public class Jio implements Recharge, DataRecharge {

	@Override
	public String dataFor50() {

		return "Internet for 6 days";
	}

	@Override
	public String dataFor100() {

		return "Internet for 10 days";
	}

	@Override
	public int rs50() {
		System.out.println("60 SMS free--Jio");
		return 45;
	}

	@Override
	public int rs100() {
		System.out.println("150 SMS free--Jio");
		return 90;
	}

}
