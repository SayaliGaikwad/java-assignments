package com.model;

public interface Recharge {

	public static final int MINAMOUNT = 5;

	public abstract int rs50();

	public abstract int rs100();

}
