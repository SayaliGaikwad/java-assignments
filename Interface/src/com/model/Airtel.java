package com.model;

public class Airtel implements Recharge, DataRecharge {

	@Override
	public String dataFor50() {

		return "Internet for 5 days";
	}

	@Override
	public String dataFor100() {

		return "Internet for 12 days";
	}

	@Override
	public int rs50() {
		System.out.println("50 SMS free--Airtel");
		return 30;
	}

	@Override
	public int rs100() {
		System.out.println("100 SMS free--Airtel");
		return 80;
	}

}
