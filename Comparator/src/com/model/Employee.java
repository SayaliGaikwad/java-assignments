package com.model;

import java.util.Comparator;

public class Employee implements Comparator<Employee> {
	private int empNo;
	private String empName;
	private float salary;

	public Employee() {
		super();

	}

	public Employee(int empNo, String empName, float salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@Override
	public int compare(Employee o1, Employee o2) {
		if (o1.getEmpNo() < o2.getEmpNo())
			return -1;
		if (o1.getEmpNo() > o2.getEmpNo())
			return 1;
		else
			return 0;
	}

}
