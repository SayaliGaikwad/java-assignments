package com.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Employee;



@RestController
public class EmployeeController {

	// @RequestMapping(value = "create", method = RequestMethod.POST)... old
	@PostMapping(value = "readbyid/{alias}")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		System.out.println(employee.getDesignation());
		ResponseEntity<Employee> responseEntity = new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
		return responseEntity;
	}

	// read by single parameter --using path variable
	// http://localhost:9090/<project_name>/???
	// @RequestMapping(value = "readbyid/{alias}", method = RequestMethod.GET)...old
	@GetMapping(value = "readbyid/{alias}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("alias") int empNo) {
		ResponseEntity<Employee> responseEntity = null;
		Employee employee = null;
		if (empNo == 10) {
			employee = new Employee(10, "Ten", "Trainer");
		}
		if (empNo == 20) {
			employee = new Employee(20, "Twenty", "Developer");
		}
		return new ResponseEntity<Employee>(employee, HttpStatus.FOUND);
	}

	@PutMapping(value = "update/{changeName}")
	public ResponseEntity<Employee> updateEmployeeById(@RequestBody Employee employee,
			@PathVariable String changeName) {
		employee.setEmpName(changeName);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete/{del}")
	public ResponseEntity<Boolean> deleteEmployeeById(@PathVariable("del") int empId) {

		return new ResponseEntity<Boolean>(true, HttpStatus.GONE);
	}
}
