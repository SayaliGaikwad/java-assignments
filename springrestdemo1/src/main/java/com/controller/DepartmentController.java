package com.controller;

import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.model.Department;
import com.model.Employee;

@RestController
public class DepartmentController {
	// handler
	@RequestMapping(value = "one", method = RequestMethod.GET, produces = { org.springframework.http.MediaType.APPLICATION_JSON_VALUE,
			org.springframework.http.MediaType.APPLICATION_XML_VALUE})
	public String sayHello() {
		return "Welcome to spring REST";
	}

	// each method in rest api is called resource
	@RequestMapping(value = "two", method = RequestMethod.GET)
	public ResponseEntity<Department> getDepartment() {
		Department department = new Department(10, "Development", "Bangalore");

		Employee employee1 = new Employee(10, "Ten", "Trainer");
		Employee employee2 = new Employee(20, "Twenty", "Tester");
		Employee employee3 = new Employee(30, "Thirty", "Developer");

		List<Employee> employeeList = new ArrayList();
		employeeList.add(employee1);
		employeeList.add(employee2);
		employeeList.add(employee3);

		department.setEmployees(employeeList);
		
		//HTTP response: status code+ headers + body
		ResponseEntity<Department> responseEntity = new ResponseEntity<Department>(department,HttpStatus.CREATED);
		return responseEntity;
	}

}
