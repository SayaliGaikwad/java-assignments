package com.sbthymeleafdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.sbthymeleafdemo.model.User;

@Controller
public class UserController {

	@GetMapping(value="/")
	public String first()
	{
		return "index";  //index=user input>> thymeleaf
	}
	
	@GetMapping(value="/save")
	public ModelAndView second(@ModelAttribute User user)
	{
		ModelAndView modelAndView= new ModelAndView();
		modelAndView.setViewName("success");
		modelAndView.addObject(user);
		return modelAndView;
	}
}
