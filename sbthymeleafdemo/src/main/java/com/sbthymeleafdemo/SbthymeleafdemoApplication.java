package com.sbthymeleafdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbthymeleafdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbthymeleafdemoApplication.class, args);
	}

}
