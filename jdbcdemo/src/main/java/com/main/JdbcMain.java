package com.main;

import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;

public class JdbcMain {

	public static void main(String[] args) {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		employeeDao.readEmployeeById(20);
		employeeDao.readEmployeeByName("Thirty");
	}

}
