package com.dao;

import com.model.Employee;

public interface EmployeeDao {

	public abstract Employee createEmployee(Employee employee);

	public abstract Employee readEmployeeById(int id);

	public abstract Employee readEmployeeByName(String name);

	public abstract Employee updateEmployee(Employee employee);

	public abstract String deleteEmployeeById(int id);

	public abstract Employee deleteEmployeeByName(String name);
}
