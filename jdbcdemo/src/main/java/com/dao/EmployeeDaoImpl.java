package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee createEmployee(Employee employee) {

		return null;
	}

	@Override
	public Employee readEmployeeById(int id) {
		String query = "SELECT *FROM emptable WHERE emp_no=?";
		Connection connection = TestMySql.getConnection();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				System.out.println("Result of employee read by id:-");
				System.out.println("Employee number= " + resultSet.getInt("emp_no"));
				System.out.println("Employee name= " + resultSet.getString("emp_name"));
				System.out.println("Employee salary= " + resultSet.getFloat("salary"));
				System.out.println("");
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Employee readEmployeeByName(String name) {
		String query = "SELECT *FROM emptable WHERE emp_name=?";
		Connection connection = TestMySql.getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				System.out.println("Result of employee read by name:-");
				System.out.println("Employee number= " + resultSet.getInt("emp_no"));
				System.out.println("Employee name= " + resultSet.getString("emp_name"));
				System.out.println("Employee salary= " + resultSet.getFloat("salary"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {

		return null;
	}

	@Override
	public String deleteEmployeeById(int id) {

		return null;
	}

	@Override
	public Employee deleteEmployeeByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
