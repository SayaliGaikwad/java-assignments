package com.hcl.pppetmicroservice.controller;

import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pppetmicroservice.exception.PetPeersException;
import com.hcl.pppetmicroservice.model.Pet;
import com.hcl.pppetmicroservice.service.PetService;
import com.hcl.pppetmicroservice.validator.PetValidator;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pets")
@Slf4j
public class PetPeersPetController {

	@Autowired
	private PetService petService;

	@GetMapping("/pethome")
	public ResponseEntity<Set<Pet>> petHome() throws PetPeersException {
		Set<Pet> pets = null;
		ResponseEntity<Set<Pet>> responseEntity = null;
		try {
			pets = petService.getAllPets();
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			log.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<Pet>>( HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	
	@GetMapping("/petDetail/{petId}")
	public ResponseEntity<Optional<Pet>> petDetailByPetId(
	@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long petId)
	throws PetPeersException {
	Optional<Pet> petDetail = null;
	ResponseEntity<Optional<Pet>> responseEntity = null;
	try {
	petDetail = petService.getPetByPetId(petId);
	if (petDetail != null) {
	responseEntity = new ResponseEntity<Optional<Pet>>(petDetail, HttpStatus.OK);
	} else {
	responseEntity = new ResponseEntity<Optional<Pet>>(petDetail, HttpStatus.NOT_FOUND);
	}
	} catch (PetPeersException e) {
	log.error("{}", e.getMessage());
	responseEntity = new ResponseEntity<Optional<Pet>>(HttpStatus.CONFLICT);
	}
	return responseEntity;
	}



	@PostMapping("/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody PetValidator petRequest) throws PetPeersException {
		Pet pets = null;
		ResponseEntity<Pet> responseEntity = null;
		try {
			pets = petService.savePet(petRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Pet>(pets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Pet>(pets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			log.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Pet>(HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping("/user/petDetail/{userId}")
	public ResponseEntity<Set<Pet>> petDetailByUserId(
			@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		Set<Pet> petDetail = null;
		ResponseEntity<Set<Pet>> responseEntity = null;
		try {
			petDetail = petService.getPetByUserId(userId);
			if (petDetail != null) {
				responseEntity = new ResponseEntity<Set<Pet>>(petDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<Pet>>(petDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			log.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<Pet>>(HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	
}

