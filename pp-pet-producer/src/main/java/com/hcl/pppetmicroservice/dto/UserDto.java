package com.hcl.pppetmicroservice.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7078857822673335724L;

	private Long id;

	private String userName;

	private String userPassword;

}

